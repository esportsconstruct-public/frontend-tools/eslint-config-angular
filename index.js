"use strict";
// IMPORTANT: Please keep these rules in alphabetical order. Add even those rules that you have disabled

module.exports = {
    overrides: [
        {
            files: ["*.ts"],
            parser: "@typescript-eslint/parser",
            parserOptions: {
                ecmaVersion: 2020,
                sourceType: "module",
            },
            plugins: ["@typescript-eslint", "@angular-eslint"],
            rules: {
                "no-restricted-imports": [
                    "error",
                    {
                        paths: [
                            {
                                name: "rxjs/Rx",
                                message: "Please import directly from 'rxjs' instead",
                            },
                        ],
                    },
                ],

                /**
                 * @see https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin/src/configs/all.json
                 */
                "@angular-eslint/component-class-suffix": "error",
                "@angular-eslint/component-max-inline-declarations": "error",
                "@angular-eslint/component-selector": "error",
                "@angular-eslint/contextual-decorator": "error",
                "@angular-eslint/contextual-lifecycle": "error",
                "@angular-eslint/directive-class-suffix": "off",
                "@angular-eslint/directive-selector": "error",
                "@angular-eslint/no-attribute-decorator": "error",
                "@angular-eslint/no-conflicting-lifecycle": "error",
                "@angular-eslint/no-empty-lifecycle-method": "error",
                "@angular-eslint/no-forward-ref": "error",
                "@angular-eslint/no-host-metadata-property": "error",
                "@angular-eslint/no-input-prefix": "error",
                "@angular-eslint/no-input-rename": "error",
                "@angular-eslint/no-inputs-metadata-property": "error",
                "@angular-eslint/no-lifecycle-call": "error",
                "@angular-eslint/no-output-native": "error",
                "@angular-eslint/no-output-on-prefix": "error",
                "@angular-eslint/no-output-rename": "error",
                "@angular-eslint/no-outputs-metadata-property": "error",
                "@angular-eslint/no-pipe-impure": "error",
                "@angular-eslint/no-queries-metadata-property": "error",
                "@angular-eslint/pipe-prefix": "error",
                "@angular-eslint/prefer-on-push-component-change-detection": "error",
                "@angular-eslint/prefer-output-readonly": "error",
                "@angular-eslint/relative-url-prefix": "error",
                /*
                Great rule (sort-ngmodule-metadata-arrays) of thumb, but --fix only moves one item and you have to run the linter many times to fix it. Enable when --fix will correct order immediately
                 */
                "@angular-eslint/sort-ngmodule-metadata-arrays": "off",
                "@angular-eslint/use-component-selector": "error",
                "@angular-eslint/use-component-view-encapsulation": "error",
                "@angular-eslint/use-injectable-provided-in": "off",
                "@angular-eslint/use-lifecycle-interface": "error",
                "@angular-eslint/use-pipe-transform-interface": "error"
            },
        },
        // {
        //     /**
        //      * @see https://github.com/angular-eslint/angular-eslint/blob/master/packages/eslint-plugin-template/src/configs/all.json
        //      */
        //     files: ["*.component.html"],
        //     "extends": ["plugin:@angular-eslint/template/recommended"],
        //     parser: "@angular-eslint/template-parser",
        //     parserOptions: {
        //         ecmaVersion: 2020,
        //         sourceType: "module",
        //     },
        //     plugins: ["@angular-eslint/template"],
        //     rules: {
        //         "@angular-eslint/template/accessibility-alt-text": "error",
        //         "@angular-eslint/template/accessibility-elements-content": "error",
        //         "@angular-eslint/template/accessibility-label-for": "error",
        //         "@angular-eslint/template/accessibility-label-has-associated-control": "error",
        //         "@angular-eslint/template/accessibility-table-scope": "error",
        //         "@angular-eslint/template/accessibility-valid-aria": "error",
        //         "@angular-eslint/template/banana-in-box": "error",
        //         "@angular-eslint/template/click-events-have-key-events": "error",
        //         "@angular-eslint/template/conditional-complexity": "error",
        //         "@angular-eslint/template/cyclomatic-complexity": "off",
        //         "@angular-eslint/template/eqeqeq": "error",
        //         "@angular-eslint/template/i18n": "error",
        //         "@angular-eslint/template/mouse-events-have-key-events": "error",
        //         "@angular-eslint/template/no-any": "error",
        //         "@angular-eslint/template/no-autofocus": "error",
        //         "@angular-eslint/template/no-call-expression": "error",
        //         "@angular-eslint/template/no-distracting-elements": "error",
        //         "@angular-eslint/template/no-duplicate-attributes": "error",
        //         "@angular-eslint/template/no-negated-async": "error",
        //         "@angular-eslint/template/no-positive-tabindex": "error",
        //         "@angular-eslint/template/use-track-by-function": "error"
        //     },
        // },
        // {
        //     files: ['*.component.ts'],
        //     extends: ['plugin:@angular-eslint/template/process-inline-templates'],
        // },
    ],
    reportUnusedDisableDirectives: true,
};
