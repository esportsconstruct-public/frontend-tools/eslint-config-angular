# ESLint config for EsportsConstruct Angular projects

A shareable [ESLint](https://eslint.org/) config for EsportsConstruct Angular projects.

## Development

You will need to set an environment variable named `EC_NPM_TOKEN` with the value of a [personal access token](https://gitlab.com/profile/personal_access_tokens) (name: `npm`, scopes: `api`).

Run `npm install` to install the project dependencies.

### Publishing a new version

1. Run `npm version patch` (replace `patch` [as necessary](https://docs.npmjs.com/cli/version)) to increase the version number.
2. Run `git push && git push --tags` to push the version commit and tag.
3. Run `npm publish` to publish the new version.
